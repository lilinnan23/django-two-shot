# from django.shortcuts import render
# from .models import Receipt

# def receipt_list(request):
#     receipts = Receipt.objects.all()
#     return render(request, 'receipts/receipt_list.html', {'receipts': receipts})


# receipts/views.py

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Receipt, ExpenseCategory, ExpenseCategoryForm
from .forms import ReceiptForm, AccountForm


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(request, 'receipts/receipt_list.html', {'receipts': receipts})


@login_required
def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('receipts:home')
    else:
        form = ReceiptForm()
    return render(request, 'receipts/create_receipt.html', {'form': form})

@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    category_data = []
    for category in categories:
        receipt_count = Receipt.objects.filter(category=category).count()
        category_data.append({'name': category.name, 'receipt_count': receipt_count})
    return render(request, 'receipts/category_list.html', {'categories': category_data})


@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect('receipts:category_list')
    else:
        form = ExpenseCategoryForm()
    return render(request, 'receipts/create_category.html', {'form': form})

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect('receipts:account_list')
    else:
        form = AccountForm()
    return render(request, 'receipts/create_account.html', {'form': form})
